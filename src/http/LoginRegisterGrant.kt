package how.kotlinhow.http

data class LoginRegister(val email: String, val pass: String) : HttpGrant()