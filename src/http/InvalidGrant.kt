package how.kotlinhow.http

data class InvalidGrant(val grantType: String, val grantResult: String) : HttpGrant()