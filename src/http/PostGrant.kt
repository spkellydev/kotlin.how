package how.kotlinhow.http

data class PostGrant(
        val id: Int,
        val title : String,
        val content : String,
        val type : String,
        val status : String,
        val slug : String,
        val prop : String?,
        val values : String?,
        val created: String,
        val modified: String?
) : HttpGrant()