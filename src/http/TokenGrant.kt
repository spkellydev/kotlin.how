package how.kotlinhow.http

data class TokenGrant(val token: String): HttpGrant()