package how.kotlinhow

import how.kotlinhow.auth.SimpleJWT
import how.kotlinhow.http.InvalidGrant
import how.kotlinhow.http.LoginRegister
import how.kotlinhow.repo.Post.PostController
import how.kotlinhow.repo.User.UserController
import how.kotlinhow.http.PostGrant
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.jwt
import io.ktor.response.*
import io.ktor.features.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.gson.*
import io.ktor.request.receive
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import java.util.logging.Level
import java.util.logging.Logger

fun initDB() {
    Database.connect("jdbc:postgresql://localhost:5432/kotlinhow", driver = "org.postgresql.Driver", user = "khow", password = "encrypted")
}

fun main(args: Array<String>) {
    embeddedServer(Netty, port = 8080, module = Application::module).start(wait = true)
}

class InvalidCredentialsException(message: String) : RuntimeException(message)
@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    val simpleJwt = SimpleJWT("my-super-secret-for-jwt")
    val logger = Logger.getAnonymousLogger()

    logger.log(Level.INFO, "application bootstrapped")
    install(Compression)
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    install(Authentication) {
        jwt {
            verifier(simpleJwt.verifier!!)
            validate {
                UserIdPrincipal(it.payload.getClaim("email").asString())
            }
        }
    }

    install(ContentNegotiation) {
        gson {
            enableComplexMapKeySerialization()
        }
    }

    install(StatusPages) {
        exception<InvalidCredentialsException> { ex ->
            call.respond(HttpStatusCode.Unauthorized, mapOf("error" to ex.message))
        }
        exception<java.lang.RuntimeException> { ex ->
            call.respond(HttpStatusCode.ExpectationFailed, mapOf("error" to ex.message))
        }
    }

    initDB()
    routing {
        auth(logger, simpleJwt)
        posts(logger)
        status(logger)
    }
}

fun Routing.auth(logger: Logger, simpleJwt: SimpleJWT) {
    route("/auth") {
        val userController = UserController(logger, simpleJwt)
        post("/signin") {
            val post = call.receive<LoginRegister>()
            call.respond(userController.signin(post))
        }

        post("/signup") {
            val post = call.receive<LoginRegister>()
            call.respond(userController.signup(post))
        }
    }
}

fun Routing.posts(logger: Logger) {
    route("/posts") {
        val postsController = PostController()
        // get all
        get("/") {
            logger.log(Level.INFO, "post get all")
            call.respond(postsController.index())
        }
        // get single by id
        get("/{id}") {
            val id = call.parameters["id"]!!.toInt()
            logger.log(Level.INFO, "post get $id")
            call.respond(postsController.single(id))
        }
        // protected routes
        authenticate {
            // create new
            post("/new") {
                val principal = call.principal<UserIdPrincipal>()
                val post = call.receive<PostGrant>()
                logger.log(Level.INFO, "post created ${post.id}")
                call.respond(postsController.create(post, principal))
            }

            // update single by id
            put("/{id}") {
                val id = call.parameters["id"]!!.toInt()
                val post = call.receive<PostGrant>()
                logger.log(Level.INFO, "post update $id")
                call.respond(postsController.update(id, post))
            }
            // delete single
            delete("/{id}") {
                val id = call.parameters["id"]!!.toInt()
                logger.log(Level.INFO, "post deletion $id")
                call.respond(postsController.delete(id))
            }
        }
    }
}

fun Routing.status(logger: Logger) {
    route("/check") {
        get("/health") {
            logger.log(Level.INFO, "health check")
            call.respond("ok")
        }
    }
}