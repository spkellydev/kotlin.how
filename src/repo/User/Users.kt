package how.kotlinhow.repo.User

import org.jetbrains.exposed.sql.Table

object Users : Table() {
    val id = integer("id").primaryKey().autoIncrement()
    val email = text("email")
    val pass = text("pass")
    val created = text("created")
    val modified = text("modified")
}