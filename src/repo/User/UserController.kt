package how.kotlinhow.repo.User

import how.kotlinhow.auth.PasswordHasher
import how.kotlinhow.auth.SimpleJWT
import how.kotlinhow.http.*
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.lang.Exception
import java.util.logging.Level
import java.util.logging.Logger

class UserController(private val logger: Logger, private val simpleJwt: SimpleJWT) {
    private val hasher = PasswordHasher()
    fun signin(post: LoginRegister) : HttpGrant {
        val grant: HttpGrant
        logger.log(Level.INFO, "querying for user")
        grant = transaction {
            try {
                val results = Users.select { Users.email eq post.email }.map { it ->
                    // temp value to make sure the database has iterable rows
                    var ok = true
                    if (it[Users.email].isEmpty()) ok = false
                    // create user object
                    val user = User(email = it[Users.email], pass = it[Users.pass])
                    val json : HttpGrant
                    json = if (ok) {
                        if (!hasher.checkPassword(post.pass, user.pass)) {
                            // bad password
                            InvalidGrant("login", "bad password")
                        } else {
                            // grant token to user
                            TokenGrant(token = simpleJwt.sign(user.email))
                        }
                    } else {
                        InvalidGrant("login", "exception found")
                    }
                    json
                }.first()
                results
            } catch (e: Exception) {
                InvalidGrant("login", "failure")
            }
        }
        return grant
    }

    fun signup(post: LoginRegister): HttpGrant {
        val userExits = try {
            transaction {
                 Users.select { Users.email eq post.email }.first()
            }
            true
        } catch (e: Exception) {
            false
        }

        return if (userExits) {
            InvalidGrant("signup", "cannot sign up user")
        } else {
            transaction {
                val hashedPw = hasher.getSaltedHash(post.pass)
                Users.insert {
                    it[Users.email] = post.email
                    it[Users.pass] = hashedPw
                }
            }
            ValidGrant("signup", "user created")
        }
    }
}