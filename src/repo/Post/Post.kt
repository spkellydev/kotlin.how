package how.kotlinhow.repo.Post

data class Post (
        val id: Int,
        val title: String,
        val content: String,
        val type: String,
        val status: String,
        val slug: String
) {
    /**
     * Secondary constructor primarily used for returning an updated post,
     * where the id needs to be consistent with the original record
     */
    constructor(id: Int, updated: Post) : this(
            id = id, title = updated.title, content = updated.content,
            type = updated.type, status = updated.status, slug = updated.slug
    )
}