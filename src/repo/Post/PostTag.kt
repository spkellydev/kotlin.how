package how.kotlinhow.repo.Post

data class PostTag (
        val postId: Int,
        val tag: String
)