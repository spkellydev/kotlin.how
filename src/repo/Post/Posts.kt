package how.kotlinhow.repo.Post

import org.jetbrains.exposed.sql.Table

object Posts : Table() {
    val id = integer("id").primaryKey().autoIncrement()
    val title = text("title")
    val content = text("content")
    val type = text("type")
    val status = text("status")
    val slug = text("slug").uniqueIndex()
}