package how.kotlinhow.repo.Post

data class PostMeta (
        val metaId: Int,
        val postId: Int,
        val prop: String?,
        val value: String,
        val created: String,
        val modified: String?
)