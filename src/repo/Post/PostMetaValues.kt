package how.kotlinhow.repo.Post

import org.jetbrains.exposed.sql.Table

object PostMetaValues : Table("post_meta") {
    val metaId = integer("meta_id").autoIncrement()
    val postId = integer("post_id")
    val prop = text("prop").nullable()
    val values = text("value").nullable()
    val created = datetime("created")
    val modified = datetime("modified").nullable()
}

object PostTags : Table() {
    val postId = integer("post_id")
    val tag = text("tag")
}