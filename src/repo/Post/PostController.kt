package how.kotlinhow.repo.Post

import how.kotlinhow.InvalidCredentialsException
import how.kotlinhow.http.PostGrant
import how.kotlinhow.repo.User.Users
import io.ktor.auth.UserIdPrincipal
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.lang.Exception
import java.lang.RuntimeException

class PostController {
    private val joinedPostAndMeta : Join = Posts.innerJoin(PostMetaValues, { Posts.id }, { PostMetaValues.postId })

    fun index(): ArrayList<PostGrant> {
        val posts: ArrayList<PostGrant> = arrayListOf()
        transaction {
            joinedPostAndMeta.selectAll().map {
                posts.add(
                    PostGrant(
                        id = it[Posts.id], content = it[Posts.content],
                        title = it[Posts.title], type = it[Posts.type],
                        status = it[Posts.status], prop = it[PostMetaValues.prop],
                        values = it[PostMetaValues.values], slug = it[Posts.slug],
                        created = it[PostMetaValues.created].toString(), modified = it[PostMetaValues.modified].toString()
                    )
                )
            }
        }
        return posts
    }

    fun create(inbound : PostGrant, principal: UserIdPrincipal?): PostGrant {
        // get user from JWT payload
        val user = try {
            transaction {
                Users.select { Users.email eq principal?.name }.first()
            }
        } catch (e: Exception) {
            throw InvalidCredentialsException("bad")
        }

        val postId = transaction {
            try {
                Posts.insert {
                    it[Posts.title] = inbound.title
                    it[Posts.content] = inbound.content
                    it[Posts.status] = inbound.status
                    it[Posts.type] = inbound.type
                    it[Posts.slug] = inbound.slug
                }.generatedKey
            } catch (e: Exception) {
                throw RuntimeException(e.message)
            }
        }

        transaction {
            PostMetaValues.insert {
                it[PostMetaValues.postId] = postId!!.toInt()
                it[PostMetaValues.prop] = "owner" // associate owner to post
                it[PostMetaValues.values] = user[Users.id].toString()
            }
        }
        return single(postId!!.toInt())
    }

    fun single(id: Int): PostGrant {
        return transaction {
            joinedPostAndMeta.select { Posts.id eq id }
                    .map {
                        PostGrant(
                            id = it[Posts.id], content = it[Posts.content],
                            title = it[Posts.title], type = it[Posts.type],
                            status = it[Posts.status], slug = it[Posts.slug],
                            prop = it[PostMetaValues.prop], values = it[PostMetaValues.values],
                            created = it[PostMetaValues.created].toString(), modified = it[PostMetaValues.modified].toString()
                        )
                    }
                    .first()
        }
    }

    fun update(id: Int, newPost: PostGrant): PostGrant {
        transaction {
            Posts.update({ Posts.id eq id }) {
                it[Posts.title] = newPost.title
                it[Posts.content] = newPost.content
                it[Posts.type] = newPost.type
                it[Posts.status] = newPost.status
                it[Posts.slug] = newPost.slug
            }
        }

        transaction {
            PostMetaValues.update({ PostMetaValues.postId eq id }) {
                it[PostMetaValues.prop] = newPost.prop
                it[PostMetaValues.values] = newPost.values
            }
        }
        return single(id)
    }

    fun delete(id: Int) {
        transaction {
            PostMetaValues.deleteWhere { PostMetaValues.postId eq id }
        }
        transaction {
            Posts.deleteWhere { Posts.id eq id }
        }
    }
}