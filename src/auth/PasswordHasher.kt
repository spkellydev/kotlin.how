package how.kotlinhow.auth

import org.apache.commons.codec.binary.Base64
import java.lang.IllegalArgumentException
import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

class PasswordHasher {
    private val iterations = 20*1000
    private val saltLength = 32
    private val keyLength = 256

    fun getSaltedHash(password: String?): String {
        val salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLength)
        return Base64.encodeBase64String(salt) + "$" + hash(password, salt)
    }

    fun checkPassword(password: String?, stored: String): Boolean {
        val saltAndHash = stored.split("$")
        val result : Boolean
        if (saltAndHash.count() != 2) {
            return false
        }
        val hashOfInput = hash(password, Base64.decodeBase64(saltAndHash[0]))
        result = hashOfInput == saltAndHash[1]
        return result
    }

    private fun hash(password: String?, salt: ByteArray): String {
        if (password.isNullOrBlank()) throw IllegalArgumentException("Empty Passwords are not allowed")
        val f : SecretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
        val key = f.generateSecret(PBEKeySpec(password!!.toCharArray(), salt, iterations, keyLength))
        return Base64.encodeBase64String(key.encoded)
    }
}