\connect kotlinhow postgres

CREATE TABLE "posts" (
  id serial PRIMARY KEY,
  title VARCHAR(355),
  content TEXT,
  type VARCHAR(255) NOT NULL,
  slug VARCHAR(255) NOT NULL,
  status VARCHAR(255) NOT NULL
);

CREATE UNIQUE INDEX post_slug ON posts (lower(slug));

CREATE TABLE "post_meta" (
  meta_id serial PRIMARY KEY,
  post_id INTEGER NOT NULL,
  prop VARCHAR(255),
  value TEXT,
  created timestamp default current_timestamp,
  modified timestamp,
  FOREIGN KEY (post_id) REFERENCES posts(id)
);

CREATE TABLE "post_tags" (
  post_id INTEGER NOT NULL,
  tag VARCHAR(255) NOT NULL,
  FOREIGN KEY (post_id) REFERENCES posts(id)
);

CREATE OR REPLACE FUNCTION update_timestamps()
RETURNS TRIGGER AS $$
BEGIN
  NEW.modified = now();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_timestamps
BEFORE UPDATE ON "post_meta"
FOR EACH ROW EXECUTE PROCEDURE update_timestamps();


CREATE USER khow WITH ENCRYPTED PASSWORD 'encrypted';
GRANT ALL PRIVILEGES ON DATABASE kotlinhow to khow;
\c kotlinhow
GRANT ALL PRIVILEGES ON TABLE posts to khow;
GRANT ALL PRIVILEGES ON TABLE post_meta to khow;
GRANT ALL PRIVILEGES ON TABLE post_tags to khow;
