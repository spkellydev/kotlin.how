CREATE TABLE "users" (
  id serial PRIMARY KEY,
  email VARCHAR(255),
  pass VARCHAR(255),
  created timestamp default current_timestamp,
  modified timestamp
);

CREATE TRIGGER update_timestamps
BEFORE UPDATE ON "users"
FOR EACH ROW EXECUTE PROCEDURE update_timestamps();