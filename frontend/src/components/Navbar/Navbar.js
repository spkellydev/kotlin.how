import React, { Component } from "react";
import "./navbar.scss";

export default class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navbar: {
        isActive: ""
      },
      mobile: {
        btnClass: ""
      }
    };
  }

  handleMenuMobilePress = el => {
    let activity,
      btnClass = "";
    if (this.state.navbar.isActive === "") {
      activity = "is-active";
      btnClass = "is-medium is-fullwidth";
    } else {
      activity = "";
      btnClass = "";
    }

    this.setState({
      navbar: {
        isActive: activity
      },
      mobile: {
        btnClass
      }
    });
  };

  get isActive() {
    return this.state.navbar.isActive;
  }

  get mobileBtnClass() {
    return this.state.mobile.btnClass;
  }

  render() {
    const isActive = this.isActive;
    const mobileBtnClass = this.mobileBtnClass;

    return (
      <nav className="navbar" role="navigation">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <img
              src="https://via.placeholder.com/250x88"
              alt="logo"
              width="250"
              height="88"
            />
          </a>

          <span
            onClick={this.handleMenuMobilePress}
            role="button"
            className={`navbar-burger burger ${isActive}`}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </span>
        </div>

        <div id="navbarBasicExample" className={`navbar-menu ${isActive}`}>
          <div className="navbar-start">
            <a href="/" className="navbar-item">
              Home
            </a>

            <a href="/expanded-documentation" className="navbar-item">
              Documentation
            </a>

            <div className="navbar-item has-dropdown is-hoverable">
              <a href="/#!" className="navbar-link">
                More
              </a>

              <div className="navbar-dropdown">
                <a href="/#!" className="navbar-item">
                  About
                </a>
                <a href="/#!" className="navbar-item">
                  Jobs
                </a>
                <a href="/#!" className="navbar-item">
                  Contact
                </a>
                <hr className="navbar-divider" />
                <a href="/#!" className="navbar-item">
                  Report an issue
                </a>
              </div>
            </div>
          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                {!this.props.authenticated ? (
                  <span
                    onClick={this.props.signupResponder}
                    className={`button is-primary ${mobileBtnClass}`}
                  >
                    <strong>Sign up</strong>
                  </span>
                ) : (
                  <div className="navbar-item has-dropdown is-hoverable">
                    <a href="/#!" className="navbar-link">
                      Admin
                    </a>

                    <div className="navbar-dropdown">
                      <a href="/admin" className="navbar-item">
                        Dashboard
                      </a>
                      <a href="/admin/blog" className="navbar-item">
                        Blog
                      </a>
                    </div>
                  </div>
                )}
                <span
                  onClick={this.props.signinResponder}
                  className={`button is-light ${mobileBtnClass}`}
                >
                  {this.props.authenticated ? "Log Out" : "Log In"}
                </span>
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}
