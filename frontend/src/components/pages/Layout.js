import React, { Component } from "react";
import Modal from "react-awesome-modal";
import axios from "axios";

class LoginAndSigninForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      pass: "",
      confirm: "",
      loading: false,
      flash: "",
      inputClass: ""
    };
  }

  componentWillUnmount() {
    this.clearTimeout();
  }

  clearTimeout() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({
      loading: true
    });

    let postable = true;
    let flash = "...posting";
    if (this.props.layoutType === "signup") {
      if (this.state.pass !== this.state.confirm) {
        postable = false;
        flash = "Passwords do not match";
      }
    }

    if (postable) {
      axios
        .post("http://localhost:8080/auth/signin", {
          email: this.state.email,
          pass: this.state.pass
        })
        .then(res => {
          console.log(res);
          let response = res.data;
          if (response.token) {
            this.setState({
              flash: "Logged In!"
            });

            this.clearTimeout();
            this.timeout = setTimeout(() => {
              this.props.handleAuthenticated(response.token);
            }, 500);
          }
        });
    } else {
      this.setState({
        flash
      });
    }
  };

  handleInput = e => {
    let { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  validatePassword = e => {
    let { value } = e.target;
    if (value.length < 6) {
      this.setState({
        flash: "Password must be at least 6 characters",
        inputClass: "is-warning"
      });
    }
  };

  handleEnterKey = e => {
    if (e.keyCode === 13) {
      this.handleSubmit(e);
    }
  };

  render() {
    return (
      <Modal
        visible={this.props.wantsAuth}
        width="440"
        height="550"
        effect="fadeInUp"
        onClickAway={() => this.props.doesntWantAuth()}
      >
        <aside style={{ padding: 10 }}>
          <span style={{ float: "right", marginBottom: 10 }}>
            <button
              onClick={this.props.doesntWantAuth}
              className="button is-dark"
            >
              X
            </button>
          </span>
          <form onKeyDown={this.handleEnterKey}>
            <React.Fragment>
              <img
                src="/static/img/kotlin-how-logo.svg"
                alt="Kotlin.how -- Create Awesome experiences with Kotlin"
              />
              <div className="has-text-centered">{this.state.flash}</div>
              <div className="field">
                <div className="control">
                  <input
                    className="input is-medium"
                    onChange={this.handleInput}
                    required={true}
                    type="text"
                    name="email"
                    placeholder="Email Address"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <input
                    className={`input is-medium ${this.state.inputClass}`}
                    onChange={this.handleInput}
                    required={true}
                    type="password"
                    name="pass"
                    onBlur={this.validatePassword}
                    placeholder="Password"
                  />
                </div>
              </div>
              {this.props.layoutType === "signup" && (
                <div className="field">
                  <div className="control">
                    <input
                      className="input is-medium"
                      onChange={this.handleInput}
                      required={true}
                      type="password"
                      name="confirm"
                      placeholder="Confirm Password"
                    />
                  </div>
                </div>
              )}
              <a
                href="/#!"
                className={`button is-dark ${this.state.loading &&
                  "is-loading"}`}
                onClick={this.handleSubmit}
              >
                Submit
              </a>
            </React.Fragment>
          </form>
        </aside>
      </Modal>
    );
  }
}

export default class Layout extends React.PureComponent {
  render() {
    const { children } = this.props;
    return (
      <React.Fragment>
        <section>
          <React.Fragment>
            {children}
            {this.props.wantsAuth && (
              <LoginAndSigninForm
                wantsAuth={this.props.wantsAuth}
                doesntWantAuth={this.props.doesntWantAuth}
                layoutType={this.props.layoutType}
                handleAuthenticated={this.props.handleAuthenticated}
              />
            )}
          </React.Fragment>
        </section>
      </React.Fragment>
    );
  }
}
