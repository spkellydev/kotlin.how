import React, { Component } from "react";
import axios from "axios";

const PostBox = ({ post }) => (
  <div className="box">
    <article className="media">
      <div className="media-left">
        <figure className="image is-64x64">
          <img
            src="https://bulma.io/images/placeholders/128x128.png"
            alt="Placeholder"
          />
        </figure>
      </div>
      <div className="media-content">
        <div className="content">
          <>
            <h3>{post.title}</h3>
            <details>
              <summary
                dangerouslySetInnerHTML={{
                  __html: `${post.content.substring(0, 150)}...`
                }}
              />
              <p
                dangerouslySetInnerHTML={{
                  __html: `${post.content}`
                }}
              />
            </details>
          </>
        </div>
        <nav className="level is-mobile">
          <div className="level-left">
            <span
              onClick={() => (window.location = `/admin/blog/${post.id}`)}
              className="level-item"
              aria-label="reply"
            >
              <span className="icon is-small">
                <i className="fas fa-pencil-alt" aria-hidden="true" />
              </span>
            </span>
            <span
              onClick={() => (window.location = `/posts/${post.id}`)}
              className="level-item"
              aria-label="retweet"
            >
              <span className="icon is-small">
                <i className="fas fa-external-link-alt" aria-hidden="true" />
              </span>
            </span>
            <span
              onClick={() => console.log("delete this")}
              className="level-item"
              aria-label="like"
            >
              <span className="icon is-small">
                <i className="fas fa-trash" aria-hidden="true" />
              </span>
            </span>
          </div>
          <span className="tag">{post.type}</span>
        </nav>
      </div>
    </article>
  </div>
);

const PostList = ({ posts }) => (
  <React.Fragment>
    {posts.map(post => (
      <PostBox key={post.slug} post={post} />
    ))}
  </React.Fragment>
);

export default class AdminBlog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      loading: true
    };
  }

  componentDidMount() {
    axios.get("http://localhost:8080/posts").then(({ data }) => {
      this.setState({
        posts: data,
        loading: false
      });
    });
  }

  render() {
    return (
      <div className="container is-fluid">
        <h1 className="is-size-2">Admin Blog</h1>

        <PostList posts={this.state.posts} />
      </div>
    );
  }
}
