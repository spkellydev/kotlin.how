import React from "react";
import { Link } from "react-router-dom";

const AdminDash = props => (
  <div>
    <h2>Admin Dashboard</h2>
    <div className="columns container is-fluid">
      <div className="column">
        <Link to="/admin/blog">
          <button className="button is-fullwidth">Blog</button>
        </Link>
      </div>
      <div className="column">
        <Link to="/admin/docs">
          <button className="button is-fullwidth">Docs</button>
        </Link>
      </div>
    </div>
  </div>
);

export default AdminDash;
