import React, { Component } from "react";

export default class NotAuthenticated extends Component {
  render() {
    return (
      <div className="has-text-centered">
        <h1>Sorry, Charley. You don't have access to this page.</h1>
      </div>
    );
  }
}
