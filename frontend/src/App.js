import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Navbar from "./components/Navbar";
import Layout from "./components/pages/Layout";
import { BrowserRouter as Router, Route } from "react-router-dom";
import routes from "./utils/routes";
import NotAuthenticated from "./components/pages/Defaults/NotAuthenticated";

class RouteTable extends React.PureComponent {
  render() {
    const props = this.props;
    console.log(this.props.authenticated);
    return (
      <Router>
        <React.Fragment>
          {props.children}
          {routes.map(route => {
            if (route.protected) {
              return (
                // You can render a <Route> in as many places
                // as you want in your app. It will render along
                // with any other <Route>s that also match the URL.
                <Route
                  key={route.path}
                  path={route.path}
                  exact={route.exact}
                  component={
                    props.authenticated ? route.main : NotAuthenticated
                  }
                />
              );
            } else {
              return (
                <Route
                  key={route.path}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              );
            }
          })}
        </React.Fragment>
      </Router>
    );
  }
}

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      wantsAuth: false,
      redirect: false,
      authenticated: false
    };
  }

  componentDidMount() {
    let user = localStorage.getItem("kotlin.how.token");
    if (user) {
      this.setState({
        authenticated: true
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.authenticated !== prevState.authenticated) {
      let user = localStorage.getItem("kotlin.how.token");
      if (user) {
        this.setState({
          authenticated: this.state.authenticated
        });
      }
    }
  }

  doesntWantAuth = e => {
    this.setState({
      wantsAuth: false
    });
  };

  signup = e => {
    this.setState({
      wantsAuth: true,
      layoutType: "signup"
    });
  };

  signin = e => {
    if (!this.state.authenticated) {
      this.setState({
        wantsAuth: true,
        layoutType: "signin"
      });
    } else {
      localStorage.clear();
      this.setState({
        authenticated: false
      });
    }
  };

  handleAuthenticated = payload => {
    localStorage.setItem("kotlin.how.token", payload);
    this.setState({ wantsAuth: false, redirect: true, authenticated: true });
  };

  get wantsAuth() {
    return this.state.wantsAuth;
  }

  render() {
    return (
      <div>
        <Navbar
          signinResponder={this.signin}
          signupResponder={this.signup}
          authenticated={this.state.authenticated}
        />
        <RouteTable authenticated={this.state.authenticated}>
          <Layout
            doesntWantAuth={this.doesntWantAuth}
            wantsAuth={this.state.wantsAuth}
            layoutType={this.state.layoutType}
            handleAuthenticated={this.handleAuthenticated}
          >
            {this.state.redirect && <Redirect to="/admin" />}
          </Layout>
        </RouteTable>
      </div>
    );
  }
}
