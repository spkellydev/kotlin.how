import React from "react";
import Homepage from "../components/pages/Home/Layout";
import { AdminDash, AdminBlog } from "../components/pages/Admin";

const routes = [
  {
    path: "/",
    exact: true,
    sidebar: () => <div>home!</div>,
    main: () => <Homepage />,
    protected: false
  },
  {
    path: "/admin",
    exact: true,
    sidebar: () => <div>admin</div>,
    main: () => <AdminDash />,
    protected: true
  },
  {
    path: "/admin/blog",
    exact: true,
    sidebar: () => <div>blog</div>,
    main: () => <AdminBlog />,
    protected: true
  }
];

export default routes;
